# README #

This repository contains code implementing Icosahedral Spatial Pyramid Matching (ISPM) as described in

"Surface-Based Spatial Pyramid Matching of Cortical Regions for Analysis of Cognitive Performance",
Kristen M Campbell, Jeffrey S Anderson, P Thomas Fletcher,
22nd International Conference on Medical Image Computing and Computer Assisted Intervention
(MICCAI), 2019. [DOI:10.1007/978-3-030-32251-9_12](https://doi.org/10.1007/978-3-030-32251-9_12)


## Documentation In Progress ##
The documentation in this repository is under development.  In the meantime, if you have questions
about ISPM code, please contact Kris Campbell (kris at sci.utah.edu).

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
