import numpy as np
import scipy.ndimage as ndimage
import scipy.linalg as linalg
from sklearn.cluster import KMeans
import SimpleITK as sitk
import csv
import os.path
from pathlib2 import Path
import subprocess
import nibabel as nib
import nilearn.surface as nils
import nilearn.plotting as nilp

import itertools
import pickle
import math
import glob

from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
import scipy.stats as sct
import networkx as nx
from sklearn import manifold
import pandas as pd
from sklearn import svm
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from sklearn.pipeline import make_pipeline

import ISPM_shape_similarity_tools as ISPM

do_preprocessing = True

#workbench_path="/Applications/workbench/bin_macosx64/"
# use the workbench build with my added histogram functionality
workbench_path = "/Users/kris/Software/workbench-build/CommandLine/wb_command.app/Contents/MacOS/"
wb_shortcuts_path = "/Users/kris/Software/wb_shortcuts-master/"
surfice_path = "/Applications/Surfice/surfice.app/Contents/MacOS/"
freesurfer_path = "/Applications/freesurfer/bin/"
mesh_atlas_path = "/Users/kris/Research/ABCD-NP/data/HCP_freesurfer_meshes/standard_mesh_atlases/resample_fsaverage/"
data_100_root = "/Users/kris/Research/ABCD-NP/results/100_training_ids/freesurfer_subjects/"
results_100_root = "/Users/kris/Research/ABCD-NP/results/100_training_ids/ispm_results/"

# naming here matches freesurfer labels
structs = {}
structs['lh'] = "CORTEX_LEFT"
structs['rh'] = "CORTEX_RIGHT"

# Get data into format needed for ISPM
# 1. Convert freesurfer data into connectome workbench format
# 2. 

# get subject ids
subjs = [os.path.split(dname)[1] for dname in glob.glob('{0}*'.format(data_100_root))]
fnames = {}
for subj in subjs:
  fnames[subj] = ISPM.filenames_for_subj(subj, data_100_root, results_100_root)

score_fnames = ISPM.filenames_for_scores("100_scores/", results_100_root)

# use following levels for 32k mesh
hierarchy_levels = [16246, 8123, 4061, 2030]
# use following levels for 164k mesh
#hierarchy_levels = [81921, 40960, 20480, 10240, 5120, 2560]
sphere_dir = results_100_root + "spheres/"
r_sphere_pattern = sphere_dir + "sphere.rh.{0}.surf.gii"
ISPM.create_dir(sphere_dir)

sphere_files = {}
sphere_files['lh'] = {}
sphere_files['rh'] = {}

# TODO decide whether to go to 164k mesh or 32k mesh
sphere_files['lh']['sphere_file'] = mesh_atlas_path + "fs_LR-deformed_to-fsaverage.L.sphere.32k_fs_LR.surf.gii"
sphere_files['rh']['sphere_file'] = mesh_atlas_path + "fs_LR-deformed_to-fsaverage.R.sphere.32k_fs_LR.surf.gii"

sphere_files['lh']['sphere_hier_files'] = []
sphere_files['rh']['sphere_hier_files'] = []

sphere_files['lh']['sphere_hier_files'] = ISPM.create_sphere_hierarchy(sphere_dir + "sphere.lh.{0}.surf.gii", hierarchy_levels)
sphere_files['rh']['sphere_hier_files'] = []
for level, l_sphere in zip(hierarchy_levels, sphere_files['lh']['sphere_hier_files']):
  r_sphere = r_sphere_pattern.format(level)
  ISPM.surface_flip_lr(l_sphere, r_sphere)
  ISPM.set_structure(l_sphere, structs["lh"])
  ISPM.set_structure(r_sphere, structs["rh"])
  sphere_files['rh']['sphere_hier_files'].append(r_sphere)

if do_preprocessing:
  for sid in subjs:
    for hemi in ['lh', 'rh']:
      try:
        ISPM.fs_surf_to_gifti(fnames[sid][hemi]['fs_sphere_file'], fnames[sid][hemi]['orig_sphere_file'], structs[hemi])
        ISPM.resample_fs_surf_to_gifti(fnames[sid][hemi]['fs_white_file'], fnames[sid][hemi]['fs_pial_file'],
                                       fnames[sid][hemi]['fs_sphere_file'], sphere_files[hemi]['sphere_file'],
                                       fnames[sid][hemi]['orig_midthick_file'], fnames[sid][hemi]['resamp_midthick_file'],
                                       fnames[sid][hemi]['resamp_sphere_file'])
        ISPM.fs_annot_to_gifti(fnames[sid][hemi]['fs_annot_file'], fnames[sid][hemi]['fs_sphere_file'], 
                               fnames[sid][hemi]['orig_label_file'], structs[hemi])
        ISPM.resample_label(fnames[sid][hemi]['orig_label_file'], fnames[sid][hemi]['orig_sphere_file'],
                            sphere_files[hemi]['sphere_file'], fnames[sid][hemi]['orig_midthick_file'], 
                            fnames[sid][hemi]['resamp_midthick_file'], fnames[sid][hemi]['resamp_label_file'])
  
        ISPM.label_to_edges(sphere_files[hemi]['sphere_file'], fnames[sid][hemi]['resamp_label_file'], 
                            fnames[sid][hemi]['border_file'], fnames[sid][hemi]['edge_file'])
        ISPM.expand_edges(fnames[sid][hemi]['resamp_label_file'], fnames[sid][hemi]['edge_file'], 35) # 35 regions in freesurfer DTK parcellation
        ISPM.edge_file_to_edge_mask_and_labeled_edge_file(fnames[sid][hemi]['edge_file'], fnames[sid][hemi]['edge_mask_file'],
                                                          fnames[sid][hemi]['edge_label_file'], fnames[sid][hemi]['resamp_label_file'])
  
        score_fnames[hemi]['hist_hier_files'].append(ISPM.create_hist_hierarchy(fnames[sid][hemi]['edge_file'], sphere_files[hemi]['sphere_file'],
                                                                                sphere_files[hemi]['sphere_hier_files'], 
                                                                                fnames[sid][hemi]['edge_hist_pattern'], hierarchy_levels, False, False))
      except Exception as err:
        print ("Exception caught while processing {0}/{1}:{2}".format(sid, hemi, err))
      except:
        print ("Unknown exception caught while processing {0}/{1}".format(sid, hemi))
    else:
        score_fnames[hemi]['hist_hier_files'].append(ISPM.create_hist_hierarchy(fnames[sid][hemi]['edge_file'], sphere_files[hemi]['sphere_file'],
                                                                                sphere_files[hemi]['sphere_hier_files'], 
                                                                                fnames[sid][hemi]['edge_hist_pattern'], hierarchy_levels, True, True))
