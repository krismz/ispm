# basic environment setup
import numpy as np
import scipy.ndimage as ndimage
import scipy.linalg as linalg
from sklearn.cluster import KMeans
#from mpl_toolkits.mplot3d import axes3d
#from mpl_toolkits.mplot3d import art3d
#import matplotlib.pyplot as plt
#from matplotlib import cm
import SimpleITK as sitk
import csv
import os.path
from pathlib2 import Path
import subprocess
import nibabel as nib
import nilearn.surface as nils
import nilearn.plotting as nilp

import itertools
import pickle
import math

from scipy.cluster.hierarchy import dendrogram, linkage
from scipy.spatial.distance import squareform
import scipy.stats as sct
import networkx as nx
from sklearn import manifold
import pandas as pd
from sklearn import svm
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from sklearn.pipeline import make_pipeline


# Need a better way than hard-coding workbench_path here
workbench_path = "/Users/kris/Software/workbench-build/CommandLine/wb_command.app/Contents/MacOS/"
wb_shortcuts_path = "/Users/kris/Software/wb_shortcuts-master/"
freesurfer_path = "/Applications/freesurfer/bin/"

def subprocess_call(cmd):
  subprocess.call(['/bin/bash', '-i', '-c', cmd])

######## wb_command tools ##########

def label_to_edges(surf_file, label_file, border_file, edge_file):
  cmd1 = "{0}/wb_command -label-to-border -placement 0.5 {1} {2} {3}".format(workbench_path, surf_file, label_file, border_file)
  subprocess.call(cmd1, shell=True)
  cmd2 = "{0}/wb_command -border-to-vertices {1} {2} {3}".format(workbench_path, surf_file, border_file, edge_file)
  subprocess.call(cmd2, shell=True)
  
def expand_edges(label_file, edge_file, num_regions=180):
  # Find labels that are all 0 and add an entry to the edge_file for that region
  labels = nils.load_surf_data(label_file)
  edge_gifti = nib.load(edge_file)
  orig_edges = nils.load_surf_data(edge_file)
  zs = np.zeros((orig_edges.shape[0]))
  intent = edge_gifti.darrays[0].intent
  for ii in reversed(range(orig_edges.shape[1])):
    edge_gifti.remove_gifti_data_array(ii)

  orig_edge_idx = 0
  for ii in range(num_regions):
    if labels[labels == (ii+1)].sum():
      gdata = nib.gifti.gifti.GiftiDataArray(data=orig_edges[:,orig_edge_idx].astype('int32'),
                                                  intent=intent)
      edge_gifti.add_gifti_data_array(gdata)
      orig_edge_idx += 1
    else:
      gdata = nib.gifti.gifti.GiftiDataArray(data=zs.astype('int32'),
                                                  intent=intent)
      edge_gifti.add_gifti_data_array(gdata)
      
  nib.save(edge_gifti, edge_file)

def border_edges_to_edge_mask_and_labeled_edges(edges):
  edge_mask = np.zeros((edges.shape[0]))
  edge_labels = np.zeros((edges.shape[0]))
  locs = np.where(edges == 1)
  edge_mask[locs[0]] = 1
  edge_labels[locs[0]] = locs[1] + 1 # because 0 means no label, so shift all labels by 1
  return(edge_mask, edge_labels)

def edge_file_to_edge_mask_and_labeled_edge_file(edge_file, edge_mask_file, edge_label_file, like_label_file):
  edges = nils.load_surf_data(edge_file)
  mask, labels = border_edges_to_edge_mask_and_labeled_edges(edges)
  save_label_as_gifti_like(mask, edge_mask_file, like_label_file)
  save_label_as_gifti_like(labels, edge_label_file, like_label_file)


def nifti_label_import(in_nifti_label_file, label_table_file, out_nifti_label_file):
  cmd1 = "{0}/wb_command -cifti-label-import {1} {2} {3}".format(workbench_path, in_nifti_label_file, label_table_file, out_nifti_label_file)
  subprocess.call(cmd1, shell=True)

def nifti_dlabel_to_gifti(nifti_dlabel_file, label, out_gifti_file):
  # label is structure like CORTEX_LEFT or CORTEX_RIGHT
  cmd1 = "{0}/wb_command -cifti-separate {1} COLUMN -label {2} {3}".format(workbench_path, nifti_dlabel_file, label, out_gifti_file)
  subprocess.call(cmd1, shell=True)

def surface_flip_lr(in_sphere_file, flip_sphere_file):
  cmd1 = "{0}/wb_command -surface-flip-lr {1} {2}".format(workbench_path, in_sphere_file, flip_sphere_file)
  subprocess.call(cmd1, shell=True)

def set_structure(surf_file, structure):
  # structure will usually be CORTEX_LEFT or CORTEX_RIGHT
  cmd1 = "{0}/wb_command -set-structure {1} {2}".format(workbench_path, surf_file, structure)
  subprocess.call(cmd1, shell=True)

def extract_regions(in_gifti_file, region_indices, out_gifti_file):
  in_gifti = nib.load(in_gifti_file)
  in_data = nils.load_surf_data(in_gifti_file)
  intent = in_gifti.darrays[0].intent
  for ii in reversed(range(len(in_gifti.darrays))):
    in_gifti.remove_gifti_data_array(ii)
  for ii in range(len(region_indices)):
    gdata =  nib.gifti.gifti.GiftiDataArray(data=in_data[:,region_indices[ii]-1].astype('int32'),
                                            intent=intent)
    in_gifti.add_gifti_data_array(gdata)
  nib.save(in_gifti, out_gifti_file)

def get_hist_hier_1d_size(hist_hier_files, levels):
  total_size = 0
  for ii in range(len(levels)):
    hist = nils.load_surf_data(hist_hier_files[ii])
    total_size += hist.shape[0] * hist.shape[1]
  return total_size

def hist_hier_to_1d_feature(hist_hier_files1, hist_hier_files2, levels):
  hist_hier1 = []
  hist_hier2 = []
  szs = []
  total_size = 0
  for ii in range(len(levels)):
    hist_hier1.append(nils.load_surf_data(hist_hier_files1[ii]))
    if hist_hier_files2:
      hist_hier2.append(nils.load_surf_data(hist_hier_files2[ii]))
    szs.append(hist_hier1[ii].shape)
    total_size += szs[ii][0] * szs[ii][1]

  if hist_hier_files2:
    total_size *= 2
  hist_feature = np.zeros((total_size))
  hist_idx = 0
  for ii in range(len(levels)):
    hist_feature[hist_idx:hist_idx + szs[ii][0] * szs[ii][1]] = hist_hier1[ii].flatten()
    hist_idx += szs[ii][0] * szs[ii][1]
    if hist_hier_files2:
      hist_feature[hist_idx:hist_idx + szs[ii][0] * szs[ii][1]] = hist_hier2[ii].flatten()
      hist_idx += szs[ii][0] * szs[ii][1]

  return hist_feature

def save_label_as_gifti_like(label_data, out_gifti_file, like_gifti_file):
  like_gifti = nib.load(like_gifti_file)
  gdata = nib.gifti.gifti.GiftiDataArray(data=label_data.astype('int32'),
                                         intent=like_gifti.darrays[0].intent)
  like_gifti.remove_gifti_data_array(0)
  like_gifti.add_gifti_data_array(gdata)
  nib.save(like_gifti, out_gifti_file)

def save_metric_as_gifti_like(metric_data, out_gifti_file, like_gifti_file):
  like_gifti = nib.load(like_gifti_file)
  gdata = nib.gifti.gifti.GiftiDataArray(data=metric_data.astype('float32'),
                                         intent=like_gifti.darrays[0].intent)
  like_gifti.remove_gifti_data_array(0)
  like_gifti.add_gifti_data_array(gdata)
  nib.save(like_gifti, out_gifti_file)

def pickle_save(thing, filename):
  with open(filename, 'wb') as f:
    pickle.dump(thing, f)
    f.close()

def pickle_load(filename):
  with open(filename, 'rb') as f:
    thing = pickle.load(f)
    f.close()
  return thing

def read_subj_ids(fname):
  with open(fname, 'r') as sf:
      ids = []
      for line in sf.readlines():
        ids.append(line.strip())
      sf.close()
      return ids

def make_sphere(num_verts, sphere_file):
  # make a sphere with approximately num_verts vertices
  cmd1 = "{0}/wb_command -surface-create-sphere {1} {2}".format(workbench_path, num_verts, sphere_file)
  subprocess.call(cmd1, shell=True)
  
def resample_surf(from_surf_file, from_sphere_file, to_sphere_file, out_surf_file):
  # downsample surface in from_surf_file based on ratio of from_sphere_file to to_sphere_file.
  # out_surf_file will be at the same resolution as to_sphere_file
  cmd1 = "{0}/wb_command -surface-resample {1} {2} {3} BARYCENTRIC {4}".format(workbench_path, from_surf_file, from_sphere_file, to_sphere_file, out_surf_file)
  subprocess.call(cmd1, shell=True)

def resample_hist(from_label_file, from_sphere_file, to_sphere_file, out_metric_file, print_commands_only=False):
#def resample_hist(from_label_file, from_sphere_file, to_sphere_file, out_metric_file, from_surf_file, to_surf_file, print_commands_only=False):
  # Compute a resampled histogram from a label file.  Right now, need a separate COLUMN in the file for each label.  Creates a metric file with COLUMNs per label containing the resampled histogram of that label
  # workbench help recommends using the midthickness surface for the area adjustment
  #cmd1 = "{0}/wb_command -hist-resample {1} {2} {3} ADAP_BARY_AREA {4} -area-surfs {5} {6}".format(workbench_path, from_label_file, from_sphere_file, to_sphere_file, out_metric_file, from_surf_file, to_surf_file)
  
  cmd1 = "{0}/wb_command -hist-resample {1} {2} {3} BARYCENTRIC {4}".format(workbench_path, from_label_file, from_sphere_file, to_sphere_file, out_metric_file)
  if print_commands_only:
    print(cmd1)
  else:
    subprocess.call(cmd1, shell=True)
  # following approach does not limit the hanging that happens
  #cmds = ["{0}/wb_command".format(workbench_path), "-hist-resample", from_label_file, from_sphere_file, to_sphere_file, "BARYCENTRIC", out_metric_file]
  #subprocess.call(cmds)

def create_sphere_hierarchy(sphere_pattern, levels, filenames_only=False):
  sphere_files = []
  for level in levels:
    sphere_file = sphere_pattern.format(level)
    if not filenames_only:
      make_sphere(level, sphere_file)
    sphere_files.append(sphere_file)
  return sphere_files

def create_surf_hierarchy(from_surf, from_sphere, to_sphere_files, surf_pattern, levels, filenames_only=False):
  out_surf_files = []
  for level, to_sphere in zip(levels, to_sphere_files):
    #surf_file = surf_prefix + "{0}.surf.gii".format(level)
    surf_file = surf_pattern.format(level)
    if not filenames_only:
      resample_surf(from_surf, from_sphere, to_sphere, surf_file)
    out_surf_files.append(surf_file)
  return out_surf_files

def create_hist_hierarchy(from_label, from_sphere, to_sphere_files, metric_pattern, levels, filenames_only=False, print_commands_only=False):
#def create_hist_hierarchy(from_label, from_sphere, to_sphere_files, metric_pattern, from_surf, to_surf_files, levels, filenames_only=False, print_commands_only=False):
  out_hist_files = []
  # following seems to not work for subsequent levels
  for level, to_sphere in zip(levels, to_sphere_files):
  #for level, to_sphere, to_surf in zip(levels, to_sphere_files, to_surf_files):
    hist_file = metric_pattern.format(level)
    if not filenames_only:
      #resample_hist(from_label, from_sphere, to_sphere, hist_file, from_surf, to_surf, print_commands_only)
      resample_hist(from_label, from_sphere, to_sphere, hist_file, print_commands_only)
    out_hist_files.append(hist_file)
  # following seems to work differently for subsequent levels
  #hist_file = metric_prefix + "{0}k.shape.gii".format(levels[0])
  #resample_hist(from_label, from_sphere, to_sphere_files[0], hist_file, from_surf, to_surf_files[0])
  #out_hist_files.append(hist_file)
  #for lidx in range(1,len(levels)):
  #  hist_file = metric_prefix + "{0}k.shape.gii".format(levels[lidx])
  #  resample_hist(out_hist_files[lidx-1], to_sphere_files[lidx-1], to_sphere_files[lidx], hist_file, to_surf_files[lidx-1], to_surf_files[lidx])
  #  out_hist_files.append(hist_file)
  return out_hist_files

def hist_intersect(hist1, hist2):
  N = np.minimum(hist1, hist2).sum()
  return(N)

def hist_diff1(hist1, hist2):
  # we want to get the one directional difference, so only look where hist1 > hist2
  where_gt = hist1 > hist2
  N = np.sum((hist1[where_gt] - hist2[where_gt])) 
  return(N)

def hist_diff2(hist1, hist2):
  # we want to get the one directional difference
  N = np.sum((hist1 - hist2)) 
  return(N)

def compute_lateralization_score(hist_hier_files1, hist_hier_files2, score_file, levels, overwrite_files=False):
  # returns positive if more lateralization with first arg, negative if more lateralized to second arg
  if (not overwrite_files) and os.path.isfile(score_file):
    with open(score_file,'r') as sf:
      P = float(sf.readline().strip('[]'))
      sf.close()
    return P
  hist1 = nils.load_surf_data(hist_hier_files1[0])
  hist2 = nils.load_surf_data(hist_hier_files2[0])
  
  P1 = (hist1 > 0).sum()
  P2 = (hist2 > 0).sum()
  #print("P1:", P1)
  #print("P2:", P2)

  # To match code in /home/sci/kris/phz_project_restore/SPM_clusters.m, rho = 0.2
  # To match Zhu2011 paper, rho = 0
  #rho = 0.2
  rho = 0.0

  pyramidLevels = len(levels)

  matchNum1 = np.zeros((pyramidLevels,1))
  matchNum2 = np.zeros((pyramidLevels,1))
  for ii in range(pyramidLevels):
    hist1 = nils.load_surf_data(hist_hier_files1[ii])
    hist2 = nils.load_surf_data(hist_hier_files2[ii])

    # sometimes the histogram sizes are not equal, off by 1-3 bins.
    # so take the smallest size 
    dim0 = min(hist1.shape[0], hist2.shape[0])
    dim1 = min(hist1.shape[1], hist2.shape[1])
    
    side1 = hist_diff1(hist1[0:dim0, 0:dim1], hist2[0:dim0, 0:dim1])
    side2 = hist_diff1(hist2[0:dim0, 0:dim1], hist1[0:dim0, 0:dim1])
    #side1 = hist_diff2(hist1[0:dim0, 0:dim1], hist2[0:dim0, 0:dim1])
    print("side1 diff:", side1)
    print("side2 diff:", side2)
    #matchNum[ii] = side1 - side2
    matchNum1[ii] = side1
    matchNum2[ii] = side2

  #print("matchNum:", matchNum)
  newMatch1 = np.zeros((pyramidLevels,1))
  newMatch2 = np.zeros((pyramidLevels,1))
  newMatch1[0] = matchNum1[0]
  newMatch2[0] = matchNum2[0]
  for ii in range(1, pyramidLevels):
    newMatch1[ii] = matchNum1[ii] - matchNum1[ii-1]
    newMatch2[ii] = matchNum2[ii] - matchNum2[ii-1]

  Pside1 = 0
  Pside2 = 0
  #print("newMatch:", newMatch)
  for ii in range(pyramidLevels):
    w = 2**(-ii) # to match matlab 1-indexing 2**(-(ii+1)+1)
    Pside1 = Pside1 + newMatch1[ii] * w
    Pside2 = Pside2 + newMatch2[ii] * w
    #print("level:", ii, "P:", P, "w:", w)

  #P = P / math.sqrt(P1 * P2)
  P = Pside1 - Pside2
  P = P / (P1 + P2)

  with open(score_file, 'w') as sf:
    sf.write("{0}".format(P))
    sf.close()
  return P

def compute_matching_score(hist_hier_files1, hist_hier_files2, score_file, levels, overwrite_files=False):
# TODO Note that in Lazebnik2006, they use a sparse representation of the histogram, where they
# track the coordinates of nonzero bins.  Might be worth implementing it that way here as it should be much faster!

  if (not overwrite_files) and os.path.isfile(score_file):
    with open(score_file,'r') as sf:
      P = float(sf.readline().strip('[]'))
      sf.close()
    return P
  hist1 = nils.load_surf_data(hist_hier_files1[0])
  hist2 = nils.load_surf_data(hist_hier_files2[0])
  
  P1 = (hist1 > 0).sum()
  P2 = (hist2 > 0).sum()
  #print("P1:", P1)
  #print("P2:", P2)

  # To match code in /home/sci/kris/phz_project_restore/SPM_clusters.m, rho = 0.2
  # To match Zhu2011 paper, rho = 0
  #rho = 0.2
  rho = 0.0

  pyramidLevels = len(levels)

  matchNum = np.zeros((pyramidLevels,1))
  for ii in range(pyramidLevels):
    hist1 = nils.load_surf_data(hist_hier_files1[ii])
    hist2 = nils.load_surf_data(hist_hier_files2[ii])

    # sometimes the histogram sizes are not equal, off by 1-3 bins.
    # so take the smallest size 
    dim0 = min(hist1.shape[0], hist2.shape[0])
    dim1 = min(hist1.shape[1], hist2.shape[1])
    
    matched = hist_intersect(hist1[0:dim0, 0:dim1], hist2[0:dim0, 0:dim1])
    unmatched = np.sum(np.abs(hist1[0:dim0, 0:dim1] - hist2[0:dim0, 0:dim1]))
    #print("matched:", matched)
    #print("unmatched:", unmatched)
    # think this reversal was done based on he way that bins was calculated originally
    #matchNum[pyramidLevels-1 - ii] = matched - rho * unmatched
    matchNum[ii] = matched - rho * unmatched

  #print("matchNum:", matchNum)
  newMatch = np.zeros((pyramidLevels,1))
  newMatch[0] = matchNum[0]
  for ii in range(1, pyramidLevels):
    newMatch[ii] = matchNum[ii] - matchNum[ii-1]

  P = 0
  #print("newMatch:", newMatch)
  for ii in range(pyramidLevels):
    w = 2**(-ii) # to match matlab 1-indexing 2**(-(ii+1)+1)
    P = P + newMatch[ii] * w
    #print("level:", ii, "P:", P, "w:", w)

  if P1 > 0 and P2 > 0:
    P = P / math.sqrt(P1 * P2)

  with open(score_file, 'w') as sf:
    sf.write("{0}".format(P))
    sf.close()
  return P

def compute_jaccard_similarity_score(seg1_file, seg2_file, score_file, overwrite_files=False):
  if (not overwrite_files) and os.path.isfile(score_file):
    with open(score_file,'r') as sf:
      P = float(sf.readline().strip('[]'))
      sf.close()
    return P
  seg1 = nils.load_surf_data(seg1_file)
  seg2 = nils.load_surf_data(seg2_file)

  P = metrics.jaccard_similarity_score(seg1, seg2)

  with open(score_file, 'w') as sf:
    sf.write("{0}".format(P))
    sf.close()
  return P

def make_file_names(results_root, scores_root, data_set, prefix, num_subjs):
  # data_set is Test or ReTest
  # prefix is language or some other characteristic
  fnames = {}
  fnames["L_score_pattern"] = scores_root + "{0}_L_".format(data_set) + "{0}_{1}_" + "{0}_score.txt".format(prefix)
  fnames["R_score_pattern"] = scores_root + "{0}_R_".format(data_set) + "{0}_{1}_" + "{0}_score.txt".format(prefix)
  fnames["L_similarity_matrix"] = scores_root + "{0}_L_{1}_similarities.pkl".format(data_set, prefix)
  fnames["R_similarity_matrix"] = scores_root + "{0}_R_{1}_similarities.pkl".format(data_set, prefix)
  fnames["L_dist_csv"] = scores_root + "{0}_L_{1}_dist.csv".format(data_set, prefix)
  fnames["R_dist_csv"] = scores_root + "{0}_R_{1}_dist.csv".format(data_set, prefix)

  fnames["L_JI_score_pattern"] = scores_root + "{0}_L_".format(data_set) + "{0}_{1}_" + "{0}_JI_score.txt".format(prefix)
  fnames["R_JI_score_pattern"] = scores_root + "{0}_R_".format(data_set) + "{0}_{1}_" + "{0}_JI_score.txt".format(prefix)
  fnames["L_JI_similarity_matrix"] = scores_root + "{0}_L_{1}_JI_similarities.pkl".format(data_set, prefix)
  fnames["R_JI_similarity_matrix"] = scores_root + "{0}_R_{1}_JI_similarities.pkl".format(data_set, prefix)
  fnames["L_JI_dist_csv"] = scores_root + "{0}_L_{1}_JI_dist.csv".format(data_set, prefix)
  fnames["R_JI_dist_csv"] = scores_root + "{0}_R_{1}_JI_dist.csv".format(data_set, prefix)

  fnames["L_edge_files"] = []
  fnames["L_edge_mask_files"] = []
  fnames["L_edge_label_files"] = []
  fnames["L_edge_hist_patterns"] = []
  fnames["R_edge_files"] = []
  fnames["R_edge_mask_files"] = []
  fnames["R_edge_label_files"] = []
  fnames["R_edge_hist_patterns"] = []

  fnames["L_label_files"] = []
  fnames["R_label_files"] = []

  for ii in range(num_subjs):
    indiv_file  = results_root + "Q1-Q6_{0}27.L.edges.{1}.{2}.32k_fs_LR.shape.gii".format(data_set, prefix, ii)
    fnames["L_edge_files"].append(indiv_file)
    indiv_file  = results_root + "Q1-Q6_{0}27.L.edge_masks.{1}.{2}.32k_fs_LR.shape.gii".format(data_set, prefix, ii)
    fnames["L_edge_mask_files"].append(indiv_file)
    indiv_file  = results_root + "Q1-Q6_{0}27.L.edge_labels.{1}.{2}.32k_fs_LR.shape.gii".format(data_set, prefix, ii)
    fnames["L_edge_label_files"].append(indiv_file)
    indiv_file  = results_root + "Q1-Q6_{0}27.L.edge_hist.{1}.{2}.".format(data_set, prefix, ii) + "{0}.shape.gii"
    fnames["L_edge_hist_patterns"].append(indiv_file)
    indiv_file = results_root + "Q1-Q6_{0}27.L.CorticalAreas_dil_Final_Final_Individual.{1}.32k_fs_LR.label.gii".format(data_set, ii)
    fnames["L_label_files"].append(indiv_file)
  
    indiv_file  = results_root + "Q1-Q6_{0}27.R.edges.{1}.{2}.32k_fs_LR.shape.gii".format(data_set, prefix, ii)
    fnames["R_edge_files"].append(indiv_file)
    indiv_file  = results_root + "Q1-Q6_{0}27.R.edge_masks.{1}.{2}.32k_fs_LR.shape.gii".format(data_set, prefix, ii)
    fnames["R_edge_mask_files"].append(indiv_file)
    indiv_file  = results_root + "Q1-Q6_{0}27.R.edge_labels.{1}.{2}.32k_fs_LR.shape.gii".format(data_set, prefix, ii)
    fnames["R_edge_label_files"].append(indiv_file)
    indiv_file  = results_root + "Q1-Q6_{0}27.R.edge_hist.{1}.{2}.".format(data_set, prefix, ii) + "{0}.shape.gii"
    fnames["R_edge_hist_patterns"].append(indiv_file)
    indiv_file = results_root + "Q1-Q6_{0}27.R.CorticalAreas_dil_Final_Final_Individual.{1}.32k_fs_LR.label.gii".format(data_set, ii)
    fnames["R_label_files"].append(indiv_file)
  
  return(fnames)

def process_and_make_hist_hiers(fname_dict, L_edge_files, R_edge_files, L_label_files, R_label_files, L_sphere_file, R_sphere_file, L_sphere_hier_files, R_sphere_hier_files, L_midthick_file, R_midthick_file, L_midthick_hier_files, R_midthick_hier_files, levels, region_indices, num_subjs):
  fname_dict["L_hist_hier_files"] = []
  fname_dict["R_hist_hier_files"] = []

  for ii in range(num_subjs):
    extract_regions(L_edge_files[ii], region_indices, fname_dict["L_edge_files"][ii])
    extract_regions(R_edge_files[ii], region_indices, fname_dict["R_edge_files"][ii])

    edge_file_to_edge_mask_and_labeled_edge_file(fname_dict["L_edge_files"][ii], fname_dict["L_edge_mask_files"][ii], fname_dict["L_edge_label_files"][ii], L_label_files[ii])
    edge_file_to_edge_mask_and_labeled_edge_file(fname_dict["R_edge_files"][ii], fname_dict["R_edge_mask_files"][ii], fname_dict["R_edge_label_files"][ii], R_label_files[ii])

    fname_dict["L_hist_hier_files"].append(create_hist_hierarchy(fname_dict["L_edge_files"][ii], L_sphere_file, L_sphere_hier_files, fname_dict["L_edge_hist_patterns"][ii], L_midthick_file, L_midthick_hier_files, levels, False, True))
    fname_dict["R_hist_hier_files"].append(create_hist_hierarchy(fname_dict["R_edge_files"][ii], R_sphere_file, R_sphere_hier_files, fname_dict["R_edge_hist_patterns"][ii], R_midthick_file, R_midthick_hier_files, levels, False, True))

  return(fname_dict)

def compute_scores(fname_dict, levels, num_subjs, overwrite=False):
  L_score_array = np.zeros((num_subjs, num_subjs))
  R_score_array = np.zeros((num_subjs, num_subjs))
  for ii in range(num_subjs):
    L_score_array[ii,ii] = 1
    R_score_array[ii,ii] = 1

  for ind1, ind2 in itertools.combinations(range(num_subjs),2):
    try:
      score = compute_matching_score(fname_dict["L_hist_hier_files"][ind1], fname_dict["L_hist_hier_files"][ind2], fname_dict["L_score_pattern"].format(ind1,ind2), levels, overwrite)
      L_score_array[ind1, ind2] = score
      L_score_array[ind2, ind1] = score
    except ValueError as err:
      print("ValueError {0} while computing L score between {1} and {2}".format(err, ind1, ind2))
    except:
      print("Unexpected error while computing L score between {0} and {1}".format(ind1, ind2))

    try:
      score = compute_matching_score(fname_dict["R_hist_hier_files"][ind1], fname_dict["R_hist_hier_files"][ind2], fname_dict["R_score_pattern"].format(ind1,ind2), levels, overwrite)
      R_score_array[ind1, ind2] = score
      R_score_array[ind2, ind1] = score
    except ValueError as err:
      print("ValueError {0} while computing R score between {1} and {2}".format(err, ind1, ind2))
    except:
      print("Unexpected error while computing R score between {0} and {1}".format(ind1, ind2))

  L_dist_array = 1.0 - L_score_array
  R_dist_array = 1.0 - R_score_array

  pickle_save(L_score_array, fname_dict["L_similarity_matrix"])
  pickle_save(R_score_array, fname_dict["R_similarity_matrix"])
  np.savetxt(fname_dict["L_dist_csv"], L_dist_array, delimiter=",")
  np.savetxt(fname_dict["R_dist_csv"], R_dist_array, delimiter=",")

  return(L_score_array, R_score_array, L_dist_array, R_dist_array)

def compute_jaccard_scores(fname_dict, num_subjs, overwrite=False):
  L_score_array = np.zeros((num_subjs, num_subjs))
  R_score_array = np.zeros((num_subjs, num_subjs))
  for ii in range(num_subjs):
    L_score_array[ii,ii] = 1
    R_score_array[ii,ii] = 1

  for ind1, ind2 in itertools.combinations(range(num_subjs),2):
    try:
      score = compute_jaccard_similarity_score(fname_dict["L_label_files"][ind1], fname_dict["L_label_files"][ind2], fname_dict["L_JI_score_pattern"].format(ind1,ind2), overwrite)
      L_score_array[ind1, ind2] = score
      L_score_array[ind2, ind1] = score
    except ValueError as err:
      print("ValueError {0} while computing L JI score between {1} and {2}".format(err, ind1, ind2))
    except:
      print("Unexpected error while computing L JI score between {0} and {1}".format(ind1, ind2))

    try:
      score = compute_jaccard_similarity_score(fname_dict["R_label_files"][ind1], fname_dict["R_label_files"][ind2], fname_dict["R_JI_score_pattern"].format(ind1,ind2), overwrite)
      R_score_array[ind1, ind2] = score
      R_score_array[ind2, ind1] = score
    except ValueError as err:
      print("ValueError {0} while computing R JI score between {1} and {2}".format(err, ind1, ind2))
    except:
      print("Unexpected error while computing R JI score between {0} and {1}".format(ind1, ind2))

  L_dist_array = 1.0 - L_score_array
  R_dist_array = 1.0 - R_score_array

  pickle_save(L_score_array, fname_dict["L_JI_similarity_matrix"])
  pickle_save(R_score_array, fname_dict["R_JI_similarity_matrix"])
  np.savetxt(fname_dict["L_JI_dist_csv"], L_dist_array, delimiter=",")
  np.savetxt(fname_dict["R_JI_dist_csv"], R_dist_array, delimiter=",")

  return(L_score_array, R_score_array, L_dist_array, R_dist_array)

def get_tSNE_projection(dist_arr, random_state):
  tsne = manifold.TSNE(n_components=2, metric="precomputed", random_state=8)
  results = tsne.fit(dist_arr)
  coords = results.embedding_
  return(coords)

def get_min_max_idx(dist_arr, title=None):
  sma = numpy.ma.masked_outside(dist_arr, 0.0000000000001, 0.9999999999, copy=False)
  dist_min_idx = np.where(dist_arr == sma.min())
  dist_max_idx = np.where(dist_arr == sma.max())
  if title:
    print(title)
  print("Min of {0} found at {1}".format(dist_arr[dist_min_idx], dist_min_idx))  
  print("Max of {0} found at {1}".format(dist_arr[dist_max_idx], dist_max_idx))  
  return(dist_min_idx, dist_max_idx)

def create_dir(dirname):
  Path(os.path.dirname(dirname)).mkdir(parents=True, exist_ok=True)

def filenames_for_hemi(subj_id, hemi, data_path, results_path):
  fnames = {}
  # freesurfer files
  fnames['fs_sphere_file'] = data_path + "surf/" + hemi + ".sphere.reg"
  fnames['fs_pial_file'] = data_path + "surf/" + hemi + ".pial"
  fnames['fs_white_file'] = data_path + "surf/" + hemi + ".white"
  fnames['fs_annot_file'] = data_path + "label/" + hemi + ".aparc.annot"
  # files for connectome workbench
  fnames['orig_sphere_file'] = results_path + subj_id + "." + hemi + ".sphere.reg.164k.surf.gii"
  fnames['resamp_sphere_file'] = results_path + subj_id + "." + hemi + ".sphere.reg.32k_fs_LR.surf.gii"
  fnames['orig_midthick_file'] = results_path + subj_id + "." + hemi + ".midthickness.164k.surf.gii"
  fnames['resamp_midthick_file'] = results_path + subj_id + "." + hemi + ".midthickness.32k_fs_LR.surf.gii"
  fnames['orig_label_file'] = results_path + subj_id + "." + hemi + ".aparc.164k.label.gii"
  fnames['resamp_label_file'] = results_path + subj_id + "." + hemi + ".aparc.32k_fs_LR.label.gii"
  fnames['border_file'] = results_path + subj_id + "." + hemi + ".edge_borders.border"
  fnames['edge_file'] = results_path + subj_id + "." + hemi + ".edges.shape.gii"
  fnames['edge_mask_file'] = results_path + subj_id + "." + hemi + ".edge_masks.shape.gii"
  fnames['edge_label_file'] = results_path + subj_id + "." + hemi + ".edge_labels.shape.gii"
  fnames['edge_hist_pattern'] = results_path + subj_id + "." + hemi + ".edge_hist.{0}.shape.gii"

  return fnames

def filenames_for_subj(subj_id, data_root, results_root):
  fnames = {}
  fnames['data_path'] = data_root + subj_id + "/"
  fnames['results_path'] = results_root + subj_id + "/"

  create_dir(fnames['data_path'])
  create_dir(fnames['results_path'])

  fnames['lh'] = filenames_for_hemi(subj_id, 'lh', fnames['data_path'], fnames['results_path'])
  fnames['rh'] = filenames_for_hemi(subj_id, 'rh', fnames['data_path'], fnames['results_path'])
  return fnames

def filenames_for_hemi_score(hemi, score_path):
  fnames = {}
  fnames['hist_hier_files'] = []
  fnames['score_pattern'] = score_path + hemi + "_{0}_{1}_score.txt"
  fnames['similarity_matrix'] = score_path + hemi + "_similarities.pkl"
  fnames['dist_csv'] = score_path + hemi + "_dist.csv"
  return fnames

def filenames_for_scores(score_dir, results_root):
  fnames = {}
  fnames['scores_path'] = results_root + score_dir + "/"
  create_dir(fnames['scores_path'])

  fnames['lh'] = filenames_for_hemi_score('lh', fnames['scores_path'])
  fnames['rh'] = filenames_for_hemi_score('rh', fnames['scores_path'])
  return fnames

def fs_surf_to_gifti(fs_surf_file, surf_file, structure=None):
  # structure will usually be CORTEX_LEFT or CORTEX_RIGHT
  cmd1 = "use_freesurfer; {0}/mris_convert {1} {2}".format(freesurfer_path, fs_surf_file, surf_file)
  subprocess_call(cmd1)
  if structure:
    set_structure(surf_file, structure)

def fs_annot_to_gifti(fs_annot_file, fs_surf_file, label_file, structure=None):
  cmd1 = "use_freesurfer; {0}/mris_convert --annot {1} {2} {3}".format(freesurfer_path, fs_annot_file, fs_surf_file, label_file)
  subprocess_call(cmd1)
  if structure:
    set_structure(label_file, structure)

def resample_fs_surf_to_gifti(fs_white_file, fs_pial_file, fs_sphere_file, match_sphere_file, orig_midthick_file, new_midthick_file, new_sphere_file):
# From https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=2ahUKEwil9dm-vYLhAhUGCjQIHf0ABzUQFjAAegQICRAC&url=https%3A%2F%2Fwiki.humanconnectome.org%2Fdownload%2Fattachments%2F63078513%2FResampling-FreeSurfer-HCP_5_8.pdf%3Fversion%3D1%26modificationDate%3D1495057205644%26api%3Dv2&usg=AOvVaw3C83OIyIP8ZQA-KU9FVwke
# B. FreeSurfer native individual data to fs_LR
# 1) Make sure you have wb_shortcuts (see prerequisites above). Run:
# wb_shortcuts -freesurfer-resample-prep <fs-white> <fs-pial> <current-freesurfer-sphere> <new-
# sphere> <midthickness-current-out> <midthickness-new-out> <current-gifti-sphere-out>
# - For <fs-white> and <fs-pial>, use the subject's native mesh white and pial surfaces, respectively (usually $SUBJECTS_DIR/<subject>/surf/*h.white or *h.pial)
# - For <current-freesurfer-sphere>, use "$SUBJECTS_DIR/<subject>/surf/*h.sphere.reg"
# - For <new_sphere>, use fs_LR-deformed_to-fsaverage.*.sphere.***k_fs_LR.surf.gii
#   - Use the appropriate hemisphere and desired resolution from standard_mesh_atlases/resample_fsaverage
# - For <midthickness-current-out>, specify a name like *h.midthickness.surf.gii
# - For <midthickness-new-out>, specify a name like
# <subject>.<hem>.midthickness.***k_fs_LR.surf.gii
# - For <current-gifti-sphere-out>, specify a name like *h.sphere.reg.surf.gii
  cmd1 = "{0}/wb_shortcuts -freesurfer-resample-prep {1} {2} {3} {4} {5} {6} {7}".format(wb_shortcuts_path, fs_white_file, fs_pial_file, fs_sphere_file,
  match_sphere_file, orig_midthick_file, new_midthick_file, new_sphere_file)
  #print(cmd1)
  subprocess.call(cmd1, shell=True)

def resample_label(orig_label_file, orig_sphere_file, match_sphere_file, orig_midthick_file, new_midthick_file, new_label_file):
  cmd1 = "{0}/wb_command -label-resample {1} {2} {3} ADAP_BARY_AREA {4} -area-surfs {5} {6}".format(workbench_path, orig_label_file, orig_sphere_file, match_sphere_file, new_label_file, orig_midthick_file, new_midthick_file)
  #print(cmd1)
  subprocess.call(cmd1, shell=True)

